import java.util.ArrayList;

public class Buscador {
	public boolean buscarEnLista(String aBuscar, ArrayList<String> lista) {
		boolean encontrado = false;
		String cadena;
		int i = 0;
		while(!encontrado && i < lista.size()) {
			cadena = lista.get(i);
			encontrado = cadena.equals(aBuscar);
			i++;
		}
		return encontrado;
	}
}
