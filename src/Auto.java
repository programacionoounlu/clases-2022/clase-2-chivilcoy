
public class Auto {
	private String color;
	
	private boolean encendido = false;
	
	public Auto(String color) {
		this.setColor(color);
	}
	
	public void encender() {
		encendido = true;
	}
	
	public boolean estaEncendido() {
		return encendido;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getColor() {
		return this.color;
	}
}
